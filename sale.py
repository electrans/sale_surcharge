# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Bool, Not
from decimal import Decimal

__all__ = ['Sale']

_ZERO = Decimal('0.0')

"""To set the number of decimals to be used, in sale_discount module it is used
discount_digits from trytond.modules.account_invoice_discount.invoice, but I
prefer to use currency_digits as in sale module. However, it isn't the best
definition for percentage fields, so I use a new definition: percentage_digits."""
percentage_digits = (1, 3)


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    gross_amount = fields.Function(
        fields.Numeric(
            "Gross amount",
            digits=(16, Eval('currency_digits', 2)),
            states={
                'readonly': Eval('state') != 'draft'
                },
            depends=['currency_digits', 'state', 'lines']),
        'get_amount')
    general_expense = fields.Numeric(
        "General expense",
        digits=percentage_digits,
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': Not(Bool(Eval('show_discounts_and_surcharges')))
            },
        depends=['state', 'show_discounts_and_surcharges'])
    industrial_benefit = fields.Numeric(
        "Industrial benefit",
        digits=percentage_digits,
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': Not(Bool(Eval('show_discounts_and_surcharges')))
            },
        depends=['state', 'show_discounts_and_surcharges'])
    subcontractor_commission = fields.Numeric(
        "Subcontractor commission",
        digits=percentage_digits,
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': Not(Bool(Eval('show_discounts_and_surcharges')))
            },
        depends=['state', 'show_discounts_and_surcharges'])
    discount = fields.Numeric(
        "Sale Discount",
        digits=percentage_digits,
        states={
            'readonly': Eval('state') != 'draft',
            'invisible': Not(Bool(Eval('show_discounts_and_surcharges')))
            },
        depends=['state', 'show_discounts_and_surcharges'])
    general_expense_amount = fields.Function(
        fields.Numeric(
            "Surcharge amount",
            states={
                'readonly': Eval('state') != 'draft',
            'invisible': Not(Bool(Eval('show_discounts_and_surcharges')))
                },
            depends=['currency_digits', 'state', 'industrial_benefit',
                     'gross_amount', 'show_discounts_and_surcharges']),
        'get_amount')
    industrial_benefit_amount = fields.Function(
        fields.Numeric(
            "Surcharge amount",
            digits=(14, Eval('currency_digits', 2)),
            states={
                'readonly': Eval('state') != 'draft',
            'invisible': Not(Bool(Eval('show_discounts_and_surcharges')))
                },
            depends=['currency_digits', 'state', 'industrial_benefit',
                     'gross_amount', 'show_discounts_and_surcharges']),
        'get_amount')
    subcontractor_commission_amount = fields.Function(
        fields.Numeric(
            "Subcontractor Commission amount",
            states={
                'readonly': Eval('state') != 'draft',
            'invisible': Not(Bool(Eval('show_discounts_and_surcharges')))
                },
            depends=['currency_digits', 'state', 'industrial_benefit',
                     'gross_amount', 'show_discounts_and_surcharges']),
        'get_amount')
    gross_amount_after_surcharges = fields.Function(
        fields.Numeric(
            " ",
            digits=(16, Eval('currency_digits', 2)),
            states={
                'readonly': Eval('state') != 'draft',
                'invisible': Not(Bool(Eval('show_discounts_and_surcharges')))
                },
            depends=['currency_digits', 'state', 'gross_amount',
                     'general_expense_amount', 'industrial_benefit_amount',
                     'show_discounts_and_surcharges']),
        'get_amount')
    discount_amount = fields.Function(
        fields.Numeric(
            "Sale discount",
            digits=(14, Eval('currency_digits', 2)),
            states={
                'readonly': Eval('state') != 'draft',
                'invisible': Not(Bool(Eval('show_discounts_and_surcharges')))
                },
            depends=['currency_digits', 'state', 'industrial_benefit',
                     'gross_amount', 'show_discounts_and_surcharges']),
        'get_amount')
    show_discounts_and_surcharges = fields.Function(
        fields.Boolean("Show Discounts/Surcharges"),
        'get_show_discounts_and_surcharges',
        setter='set_show_discounts_and_surcharges'
    )

    # Override the original core function
    def get_tax_amount(self):
        original_tax_amount = super(Sale, self).get_tax_amount()
        surcharge = (self.general_expense or _ZERO) + (self.industrial_benefit or _ZERO)
        discount = (self.discount or _ZERO)
        tax_amount = original_tax_amount * (Decimal(1.0) + surcharge) * (Decimal(1.0) - discount)
        return self.currency.round(tax_amount)

    # Override the original core function
    @classmethod
    def get_amount(cls, sales, names):
        gross_amount = {}
        general_expense_amount = {}
        industrial_benefit_amount = {}
        subcontractor_commission_amount = {}
        gross_amount_after_surcharges = {}
        discount_amount = {}
        untaxed_amount = {}
        tax_amount = {}
        total_amount = {}

        if {'gross_amount', 'general_expense_amount', 
            'industrial_benefit_amount', 'gross_amount_after_surcharges',
            'discount_amount', 'subcontractor_commission_amount'} & set(names):
            names_in_intermediate_amounts = True
        else:
            names_in_intermediate_amounts = False

        if {'tax_amount', 'total_amount'} & set(names):
            compute_taxes = True
        else:
            compute_taxes = False

        # Sort cached first and re-instanciate to optimize cache management
        sales = sorted(sales, key=lambda s: s.state in cls._states_cached,
            reverse=True)
        sales = cls.browse(sales)

        def compute_intermediate_amounts(sale):
            # Gross amount (in core untaxed_amount had this value)
            gross_amount[sale.id] = sum(
                (line.amount for line in sale.lines
                    if line.type == 'line'), _ZERO)
            
            # Sale surcharges
            general_expense_amount[sale.id] = sale.currency.round(
                gross_amount[sale.id] * (sale.general_expense or _ZERO))
            industrial_benefit_amount[sale.id] = sale.currency.round(
                gross_amount[sale.id] * (sale.industrial_benefit or _ZERO))
            subcontractor_commission_amount[sale.id] = sale.currency.round(
                gross_amount[sale.id] * (sale.subcontractor_commission or _ZERO))
            gross_amount_after_surcharges[sale.id] = (gross_amount[sale.id]
                + general_expense_amount[sale.id] + industrial_benefit_amount[sale.id] + \
                subcontractor_commission_amount[sale.id])
            
            # Sale discount
            discount_amount[sale.id] = sale.currency.round(
                (gross_amount_after_surcharges[sale.id]) * (sale.discount or _ZERO))

        for sale in sales:

            # Untaxed, taxed and total amounts
            if (sale.state in cls._states_cached
                    and sale.untaxed_amount_cache is not None
                    and sale.tax_amount_cache is not None
                    and sale.total_amount_cache is not None):
                untaxed_amount[sale.id] = sale.untaxed_amount_cache
                if compute_taxes:
                    tax_amount[sale.id] = sale.tax_amount_cache
                    total_amount[sale.id] = sale.total_amount_cache
                if names_in_intermediate_amounts:
                    compute_intermediate_amounts(sale)
            else:
                compute_intermediate_amounts(sale)
                untaxed_amount[sale.id] = gross_amount[sale.id] \
                    + general_expense_amount[sale.id] + industrial_benefit_amount[sale.id] \
                    + subcontractor_commission_amount[sale.id] - discount_amount[sale.id]
                if compute_taxes:
                    tax_amount[sale.id] = sale.get_tax_amount()
                    total_amount[sale.id] = (
                        untaxed_amount[sale.id] + tax_amount[sale.id])

        res = {
            'gross_amount': gross_amount,
            'general_expense_amount': general_expense_amount,
            'industrial_benefit_amount': industrial_benefit_amount,
            'subcontractor_commission_amount': subcontractor_commission_amount,
            'gross_amount_after_surcharges': gross_amount_after_surcharges,
            'discount_amount': discount_amount,
            'untaxed_amount': untaxed_amount,
            'tax_amount': tax_amount,
            'total_amount': total_amount,
            }

        for key in list(res.keys()):
            if key not in names:
                del res[key]
        return res

    def get_show_discounts_and_surcharges(self, name):
        return False

    def set_show_discounts_and_surcharges(self, sale, name):
        'Dummy function to enable the check'
        pass