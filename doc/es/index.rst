Sale Surcharge Module
#####################

por J.A. Arias / O. Queraltó (Electrans), 2019

[APP-957] Se pueda añadir "Gastos generales" y "Beneficio industrial" a la oferta/pedido de venta

discount field replaces the sale_discount one (sale_discount module)
because we want not to compute its value in lines.